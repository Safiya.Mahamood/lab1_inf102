package INF102.lab1.triplicate;

import INF102.lab1.triplicate.ITriplicate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    private HashMap<T, Integer> type = new HashMap<>(); // index, number
    private List<T> triplicates = new ArrayList<>(); // new list to add the triplicate

    @Override
    public T findTriplicate(List<T> list) {
        for (T item : list) { // looping through items in list
            type.put(item, type.getOrDefault(item, 0) + 1); // item store key values to track the count of occurences for each element in the list
            // if the key item is not in the map it returns a default value of 0, the retrieved count is then incremented by 1 to indicate that there's on more occurence of the item in the list
            if (type.get(item) == 3 && !triplicates.contains(item)) {
                triplicates.add(item);
            }
        }
        return (T) triplicates;
    }

}